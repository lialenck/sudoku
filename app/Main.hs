{-# LANGUAGE LambdaCase #-}

module Main (main) where

import Prelude hiding (getLine)
import qualified Prelude as P

import Control.Monad
import qualified Data.Set as S
import Data.Char (chr, ord)
import Data.Maybe (catMaybes, isNothing, fromJust)
import Data.List (foldl1', minimumBy, intersperse)
import Data.List.Split (chunksOf)

type Sudoku = [Maybe Int]

for :: [a] -> (a -> b) -> [b]
for = flip map

parseSudokuLine :: String -> Sudoku
parseSudokuLine l =
    if length l /= 9
    then error "Wrong line length (expected 9)"
    else for l $ \case
        ' ' -> Nothing
        c -> Just $! ord c - ord '0'

readSudoku :: IO [Maybe Int]
readSudoku = concat <$> replicateM 9 (parseSudokuLine <$!> P.getLine)

getLine, getColumn, getSquare :: Sudoku -> Int -> Int -> [Maybe Int]
getLine f _ y = take 9 $! drop (9*y) f
getColumn f x _ = for [0..8] $ \y -> f !! (y * 9 + x)
getSquare f x y =
    let xoff = x `div` 3 * 3
        yoff = y `div` 3 * 3
    in for ((,) <$> [0..2] <*> [0..2]) $ \(xd, yd) ->
        f !! (xoff + xd + 9 * (yoff + yd))

possibleNums :: Sudoku -> Int -> Int -> S.Set Int
possibleNums field x y = foldl1' S.intersection $!
    for [getLine, getColumn, getSquare] $! \f ->
        S.difference (S.fromList [1..9]) $ S.fromList $ catMaybes $ f field x y

replaceAt :: Int -> [a] -> a -> [a]
replaceAt n l x = case splitAt n l of
    (as, _:bs) -> as ++ x : bs
    _ -> error $ "replaceAt: index " ++ show n ++ " out of range"

solve :: Sudoku -> [[Int]]
solve s =
    let emptyCoords = filter (isNothing . (s !!)) [0..80]
        moves       = for emptyCoords $ \n -> (n, possibleNums s (n `mod` 9) (n `div` 9))
        bestMoves   = minimumBy (\(_, a) (_, b) -> compare (length a) (length b)) moves

    in if emptyCoords == []
    then [map fromJust s] -- fine: should all be Justs, as our code is broken otherwise
    else case S.toList <$> bestMoves of
        (_, []) -> [] -- can't find any solutions for this field; give up
        (n, newNums) -> map (replaceAt n s . Just) newNums >>= solve

intersperseEvery :: Int -> [a] -> [a] -> [a]
intersperseEvery n a l = a ++ (concat $ intersperse a $ chunksOf n l) ++ a

showSolution :: [Int] -> String
showSolution l = intersperseEvery ((9+4+1) * 3) "+---+---+---+\n"
    $ concat $ map showRow $ chunksOf 9 $ map showInt l
    where showRow = (++"\n") . intersperseEvery 3 "|"

showInt :: Int -> Char
showInt = chr . (+ ord '0')

main :: IO ()
main = do
    s <- readSudoku
    mapM_ putStrLn $ map showSolution $ take 3 $ solve s
